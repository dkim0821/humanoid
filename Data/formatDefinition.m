clc
clear all
close all

x = importdata('xData.mat');
y = importdata('vxData.mat');

fileID = fopen('DataNewTrain','w');

for i = 1 : size(x,2)
formatSpec = '%.4f 1: %.4f\n';
fprintf(fileID, formatSpec, y(i), x(i));
end

fclose(fileID);


x = 0:0.01:1;
y = ones(length(x), 1);

fileID = fopen('DataNewTest','w');

for i = 1 : size(x,2)
formatSpec = '%.4f 1: %.4f\n';
fprintf(fileID, formatSpec, y(i), x(i));
end

fclose(fileID);


