

#ifndef __SVM_C_h__
#define __SVM_C_h__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include "svm.h"

#define Malloc(type,n) (type *)malloc((n)*sizeof(type))


svm_model* svmTrain(int argc, char **argv);
static char* readline(FILE *input);
void print_null(const char *s);
void exit_with_help();
void exit_input_error(int line_num);
void do_cross_validation();
void parse_command_line(int argc, char **argv, char *input_file_name, char *model_file_name);
void read_problem(const char *filename);
int print_null(const char *s,...);
static char* readline(FILE *input);
void predict(FILE *input, FILE *output);
int svmPredict(int argc, char **argv);



#endif
