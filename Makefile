CXX ?= g++
CFLAGS = -Wall -Wconversion -O3 -fPIC
SHVER = 2
OS = $(shell uname)

all: SMC svm-scale

lib: svm.o
	if [ "$(OS)" = "Darwin" ]; then \
		SHARED_LIB_FLAG="-dynamiclib -Wl,-install_name,libsvm.so.$(SHVER)"; \
	else \
		SHARED_LIB_FLAG="-shared -Wl,-soname,libsvm.so.$(SHVER)"; \
	fi; \
	$(CXX) $${SHARED_LIB_FLAG} svm.o svm_c.o -o libsvm.so.$(SHVER)

SMC: SMC.cpp svm.o svm_c.o  
	$(CXX) $(CFLAGS) SMC.cpp svm.o svm_c.o -o SMC -lm
svm-scale: svm-scale.c
	$(CXX) $(CFLAGS) svm-scale.c -o svm-scale
svm_c.o: svm_c.c svm_c.h svm.h
	$(CXX) $(CFLAGS) -c svm_c.c
svm.o: svm.cpp svm.h
	$(CXX) $(CFLAGS) -c svm.cpp
clean:
	rm -f *~ svm.o svm_c.o SMC svm-scale libsvm.so.$(SHVER)
