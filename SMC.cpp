        #include "svm_c.h"
        #include <string>
        #include <iostream>
        #include <cstring>
        #include <stdio.h>
        #include <cmath>
        #include <vector>
        #include <fstream>
        #define PI 3.1415926

        using namespace std;

        char **arv;
        int arc;
        void parse_param(string s);

        int main(int argc, char **argv){

           // Initialize the real data
        vector<double> xReal;
        vector<double> xRealdot;
        vector<double> xRealddot;
        vector<double> zReal;
        vector<double> zRealdot;
        vector<double> zRealddot;

        vector<double> sC1;
        vector<double> sC2;

        vector<double> uX;
        vector<double> uZ;
        vector<double> uXNumerator;
        vector<double> uXDenominator;
        vector<double> uZNumerator;
        vector<double> uZDenominator;

        double Aexp1 = 0;
        double Aexp2 = 0;

        double r = 0;//0.01;
        double theta = 3.1415926/4;

        double eta1 = 10;
        double eta2 = 20;
        double beta1 = 2;
        double beta2 = 2;

        double UMaxX = 5;
        double UMinX = -5;
        double UMaxZ = 20;
        double UMinZ = -20;

        vector<double> inc_t;
        inc_t.push_back(0.0035);
        int i = 2;
        int j = 1;

        double data;

        vector<double> x;
        vector<double> xdot;
        vector<double> z;
        vector<double> zdot;

        vector <double> t;

        ifstream FootXNormal;
        ifstream FootXdotNormal;
        ifstream FootZNormal;
        ifstream FootZdotNormal;

        FootXNormal.open("FootData/footx.txt");
        FootXdotNormal.open("FootData/footxdot.txt");
        FootZNormal.open("FootData/footz.txt");
        FootZdotNormal.open("FootData/footzdot.txt");

        while( FootXNormal >> data )
            x.push_back(data);

        while( FootXdotNormal >> data )
            xdot.push_back(data);

        while( FootZNormal >> data )
            z.push_back(data);

        while( FootZdotNormal >> data )
            zdot.push_back(data);

        FootXNormal.close();
        FootXdotNormal.close();
        FootZNormal.close();
        FootZdotNormal.close();

        char train_file_name1[1024];
        char test_file_name1[1024];
        char model_file_name1[1024];
        char output_file_name1[1024];    

        char train_file_name2[1024];
        char test_file_name2[1024];
        char model_file_name2[1024];
        char output_file_name2[1024];    

        strcpy(train_file_name1, argv[1]);
        strcpy(test_file_name1, argv[2]);
        strcpy(model_file_name1, argv[3]);
        strcpy(output_file_name1, argv[4]);

        strcpy(train_file_name2, argv[5]);
        strcpy(test_file_name2, argv[6]);
        strcpy(model_file_name2, argv[7]);
        strcpy(output_file_name2, argv[8]);

        // model 1
        string s = "svmTrain -s 3 -c 50 -t 2 -p 0.01 -g 10 ";
        s.append(train_file_name1);
        s.append(" ");
        s.append(model_file_name1);

        // printf("%s\n",output_file_name);
        parse_param(s);
        //    struct svm_param par = parse_param(s);

        svm_model *model1 = svmTrain(arc, arv);

        std::vector<double> sv1_0;
        std::vector<double> sv1_1;

        cout << "model 1" << endl;
        for(j = 0; j < model1->l;j++)
        {
         // printf("x[0] %d %f, x[1] %d %f \n",model1->SV[j][0].index, model1->SV[j][0].value,model1->SV[j][1].index, model1->SV[j][1].value);
            sv1_0.push_back(model1->SV[j][0].value);
        sv1_1.push_back(model1->SV[j][1].value);
        printf("x[0] %f, x[1] %f \n",sv1_0.back(),sv1_1.back());

        }

        cout << model1->l << endl;

        // model 2
        string s2 = "svmTrain -s 3 -c 50 -t 2 -p 0.01 -g 10 ";
        s2.append(train_file_name2);
        s2.append(" ");
        s2.append(model_file_name2);

        // printf("%s\n",output_file_name);
        parse_param(s2);
        //    struct svm_param par = parse_param(s);

        svm_model *model2 = svmTrain(arc, arv);

        std::vector<double> sv2_0;
        std::vector<double> sv2_1;
        std::vector<double> sv2_2;
        std::vector<double> sv2_3;

        cout << "model 2" << endl;
        for(j = 0; j < model2->l;j++)
        {
         // printf("x[0] %d %f, x[1] %d %f \n",model1->SV[j][0].index, model1->SV[j][0].value,model1->SV[j][1].index, model1->SV[j][1].value);
        sv2_0.push_back(model2->SV[j][0].value);
        sv2_1.push_back(model2->SV[j][1].value);
        sv2_2.push_back(model2->SV[j][2].value);
        sv2_3.push_back(model2->SV[j][3].value);
        printf("x[0] %f, x[1] %f, x[2] %f, x[3] %f \n",sv2_0.back(),sv2_1.back(),sv2_2.back(),sv2_3.back());

        }

        // string s1 = "svmPredict ";
        // s1.append(test_file_name);
        // s1.append(" ");
        // s1.append(model_file_name);
        // s1.append(" ");
        // s1.append(output_file_name);
        // //cout<<s1<<endl;

        // parse_param(s1);
        // svmPredict(arc, arv);

        //    // calculate th
        //    double ** alpha = model->sv_coef;
        //    struct svm_node ** sv = model->SV;
        //    int *nSV = model->nSV;

            // read data from txt file

        cout << x[0] << endl;
        xReal.push_back(x[0] + r * cos(theta));
        xRealdot.push_back(xdot[0] + 4 * r * sin(theta));
        xRealddot.push_back(0);
        zReal.push_back(z[0] + 0.8 * r * cos(theta));
        zRealdot.push_back(zdot[0] + 6 * r * sin(theta));
        zRealddot.push_back(0);

        // cout << "xReal" << xReal[0] << endl;
        // cout << "xRealdot" << xRealdot[0] << endl;

        sC1.push_back(- model1->rho[0]);
        // cout << "sC1" << sC1[0] << endl;
        for(j = 0; j < model1->l;j++)
        {
            Aexp1 = exp(- model1->param.gamma * (pow(xReal[0] - sv1_0[j], 2.0) + pow(xRealdot[0] - sv1_1[j], 2.0)));
            sC1[0] = sC1[0] + model1->sv_coef[0][j] * Aexp1;
            // cout << model1->SV[j][0].index << "\t" << model1->SV[j][0].value << "\t" << model1->SV[j][1].index << "\t" << model1->SV[j][1].value << endl;
         // printf("x[0] %d %f, x[1] %d %f \n",model1->SV[j][0].index, model1->SV[j][0].value,model1->SV[j][1].index, model1->SV[j][1].value);     
        }

        // cout << "sC1" << sC1[0] << endl;

        sC2.push_back(- model2->rho[0]);
        for(j = 0; j < model2->l; j++){
            Aexp2 = exp(- model2->param.gamma * (pow(xReal[0] - sv2_0[j], 2.0) + pow(xRealdot[0] - sv2_1[j], 2.0) + pow(zReal[0] - sv2_2[j], 2.0) + pow(zRealdot[0] - sv2_3[j], 2.0)));
            sC2[0] = sC2[0] + model2->sv_coef[0][j] * Aexp2;
        }

        uXNumerator.push_back(- eta1 * tanh(beta1 * sC1[0]));
        uXDenominator.push_back(0);
        Aexp1 = 0;
        for(j = 0; j < model1->l;j++)
        {
            Aexp1 = exp(- model1->param.gamma * (pow(xReal[0] - sv1_0[j], 2.0) + pow(xRealdot[0] - sv1_1[j], 2.0)));
            uXNumerator[0] = uXNumerator[0] + 2 * model1->param.gamma * model1->sv_coef[0][j] * (xReal[0] - sv1_0[j]) * xRealdot[0] * Aexp1;
            uXDenominator[0] = uXDenominator[0] - 2 * model1->param.gamma * model1->sv_coef[0][j] * (xRealdot[0] - sv1_1[j]) * Aexp1;
        }
        uX.push_back(uXNumerator[0] / uXDenominator[0]);

        // cout << "Part:" << uXNumerator[0] << uXDenominator[0] << endl;
        uZNumerator.push_back(- eta2 * tanh(beta2 * sC2[0]));
        uZDenominator.push_back(0);
        Aexp2 = 0;

        for(j = 0; j < model2->l;j++){
            Aexp2 = exp(- model2->param.gamma * (pow(xReal[0] - sv2_0[j], 2.0) + pow(xRealdot[0] - sv2_1[j], 2.0) + pow(zReal[0] - sv2_2[j], 2.0) + pow(zRealdot[0] - sv2_3[j], 2.0)));
            uZNumerator[0] = uZNumerator[0] + 2 * model2->param.gamma * model2->sv_coef[0][j] * ((xReal[0] - sv2_0[j]) * xdot[0] + (xRealdot[0] - sv2_1[j]) * uX[0] + (zReal[0] - sv2_2[j]) * zRealdot[0]) * Aexp2;
            uZDenominator[0] = uZDenominator[0] - 2 * model2->param.gamma * model2->sv_coef[0][j] * (zRealdot[0] - sv2_3[j]) * Aexp2;
        }

        vector <double> uReach;
        vector <double> uEq;

        double uZRatio = 1;
        uZ.push_back(uZRatio * uZNumerator[0] / uZDenominator[0]);
        uReach.push_back(uZRatio * (-eta2) * tanh(beta2 * sC2[0]) / uZDenominator[0]);
        uEq.push_back(uZRatio * (uZNumerator[0] - eta2 * tanh(beta2 * sC2[0])) / uZDenominator[0]);

        cout << "For X" << uXNumerator[0] << "\t" << uXDenominator[0] << "\t" << endl;
        cout << "For Z" << uZNumerator[0] << "\t" << uZDenominator[0] << "\t" << endl;

        if(abs(sC1[0]) + abs(sC2[0]) <= 1e-4){
            uX[0] = 0;
            uZ[0] = 0;
        }

        // if (uX[0] > UMaxX){
        //     uX[0] = UMaxX;
        // }else if (uX[0] < UMinX){
        //     uX[0] = UMinX;
        // }else{
        // uX[0] = uX[0];
        // }

        // if ((xReal[0] <= 0.02) && (uX[0] < 0))
        // {   
        // uX[0] = - uX[0];
        // }
        // cout << uX[0] << endl;

        // if (uZ[0] > UMaxZ){
        // uZ[0] = UMaxZ;
        // }else if(uZ[0] < UMinZ){
        // uZ[0] = UMinZ;
        // }else{
        // uZ[0] = uZ[0];
        // }

        i = 1;
        j = 1;
        t.push_back(0);
        bool flag = 1;
        double Uix = 0.05;
        double Uiz = 0.05;

        // cout << "worksworksworksworksworks" << endl;


        // cout << xReal.back() << endl;
        // cout << xRealdot.back() << endl;
        // cout << x.back() << endl;
        // cout << xdot.back() << endl;

        // cout << "criteria" << pow(xReal.back() - x.back(), 2.0) + pow(xRealdot.back() - xdot.back(), 2.0) << endl;

        cout << uX[0] << "\t" << uZ[0] << endl;
        
            // while k <= size(x,1) -1
            //         while xReal(end) <= x(end)
            //         while (zReal(end) - z(end))^2 + (zRealdot(end) - zdot(end))^2 >= 1e-2
        while(pow(xReal.back() - x.back(), 2.0) + pow(xRealdot.back() - xdot.back(), 2.0) >= 1e-4){
        xRealdot.push_back(xRealdot[i-1] + uX[i-1] * inc_t[i-1]);
        xReal.push_back(xReal[i-1] + xRealdot[i] * inc_t[i-1] + uX[i-1] * pow(inc_t[i-1], 2.0));
        zRealdot.push_back(zRealdot[i-1] + uZ[i-1] * inc_t[i-1]);
        zReal.push_back(zReal[i-1] + zRealdot[i] * inc_t[i-1] + uZ[i-1] * pow(inc_t[i-1], 2.0));

                // if (xRealdot[i] >= 0.5 && flag == 1){
                //     xRealdot[i] = xRealdot[i] + 0.2;
                //     zRealdot[i] = zRealdot[i] - 0.3;
                //     flag = 0;
                // }

           // new surface definition
        sC1.push_back(- model1->rho[0]);
        for(j = 0; j < model1->l;j++){
           Aexp1 = exp(- model1->param.gamma * (pow(xReal[i] - sv1_0[j], 2.0) + pow(xRealdot[i] - sv1_1[j], 2.0)));
           sC1[i] = sC1[i] + model1->sv_coef[0][j] * Aexp1;
        }

        sC2.push_back(- model2->rho[0]);
        for(j = 0; j < model2->l;j++){
            Aexp2 = exp(- model2->param.gamma * (pow(xReal[i] - sv2_0[j], 2.0) + pow(xRealdot[i] - sv2_1[j], 2.0) + pow(zReal[i] - sv2_2[j], 2.0) + pow(zRealdot[i] - sv2_3[j], 2.0)));
            sC2[i] = sC2[i] + model2->sv_coef[0][j] * Aexp2;
        }

           // new surface definition
        uXNumerator.push_back(-eta1 * tanh(beta1 * sC1[i]));
        uXDenominator.push_back(0);
        Aexp1 = 0;
        for(j = 0; j < model1->l;j++){
           Aexp1 = exp(- model1->param.gamma * (pow(xReal[i] - sv1_0[j], 2.0) + pow(xRealdot[i] - sv1_1[j], 2.0)));
           uXNumerator[i] = uXNumerator[i] + 2 * model1->param.gamma * model1->sv_coef[0][j] * (xReal[i] - sv1_0[j]) * xRealdot[i] * Aexp1;
           uXDenominator[i] = uXDenominator[i] - 2 * model1->param.gamma * model1->sv_coef[0][j] * (xRealdot[i] - sv1_1[j]) * Aexp1;
           // printf("x[0] %d %f, x[1] %d %f \n",model1->SV[j][0].index, model1->SV[j][0].value,model1->SV[j][1].index, model1->SV[j][1].value);
         // printf("x[0] %f, x[1] %f \n",sv1_0[j],sv1_1[j]);

        }
        // cout << "Denominator" << uXDenominator[i] << "\t" << uXNumerator[i] << endl;

        uX.push_back(uXNumerator[i] / uXDenominator[i]);

        uZNumerator.push_back(-eta2 * tanh(beta2 * sC2[i]));
        uZDenominator.push_back(0);
        Aexp2 = 0;
        for (j = 0; j < model2->l;j++){
            Aexp2 = exp(- model2->param.gamma * (pow(xReal[i] - sv2_0[j], 2.0) + pow(xRealdot[i] - sv2_1[j], 2.0) + pow(zReal[i] - sv2_2[j], 2.0) + pow(zRealdot[i] - sv2_3[j], 2.0)));
            uZNumerator[i] = uZNumerator[i] + 2 * model2->param.gamma * model2->sv_coef[0][j] * ((xReal[i] - sv2_0[j]) * xRealdot[i] + (xRealdot[i] - sv2_1[j]) * uX[i] + (zReal[i] - sv2_2[j]) * zRealdot[i]) * Aexp2;
            uZDenominator[i] = uZDenominator[i] - 2 * model2->param.gamma * model2->sv_coef[0][j] * (zRealdot[i] - sv2_3[j]) * Aexp2;
        }

        uZ.push_back(uZRatio * uZNumerator[i] / uZDenominator[i]);
        uReach.push_back(uZRatio * (-eta2) * tanh(beta2 * sC2[i]) / uZDenominator[i]);
        uEq.push_back(uZRatio * (uZNumerator[i] - eta2 * tanh(beta2 * sC2[i])) / uZDenominator[i]);

        //Saturation Control Input X
        // if(uX[i] > UMaxX){
        //    uX[i] = UMaxX;
        // }else if(uX[i] < UMinX){
        //    uX[i] = UMinX;
        // }else{
        //    uX[i] = uX[i];
        // }

        // if(xReal[i] <= Uix && uX[i] < 0)
        //    uX[i] = - uX[i];

        // cout << "Control Input" << uX[i] << endl;
        inc_t.push_back(0.001);

        // Control Z
        //Saturation Control Input X
        // if (uZ[i] > UMaxZ){
        //     uZ[i] = UMaxZ;
        // }else if(uZ[i] < UMinZ){
        //     uZ[i] = UMinZ;
        // }else{
        //     uZ[i] = uZ[i];
        // }

        // if(zReal[i] <= Uiz && uZ[i] < 0)
        //     uZ[i] = - uZ[i];
        

        // cout << "Real Data" << xReal.back() << endl;
        // cout << "Real Datadot" << xRealdot.back() << endl;

        t.push_back(t[i-1] + inc_t[i-1]);
        ++i;
        // cout << xReal[i] << endl;
        }
        cout << "Total Iterations:" << i << endl;

        // Save the Real X Data
        ofstream foutx("CheckX.dat");

        for(unsigned int i=0; i<xReal.size(); i++)
        foutx << xReal[i] << endl;

        foutx.close();

        ofstream foutxdot("CheckXdot.dat");

        for(unsigned int i=0; i<xRealdot.size(); i++)
        foutxdot << xRealdot[i] << endl;

        foutxdot.close();

        // Save the Real Z Data
        ofstream foutz("CheckZ.dat");

        for(unsigned int i=0; i<zReal.size(); i++)
        foutz << zReal[i] << endl;

        foutz.close();

        ofstream foutzdot("CheckZdot.dat");

        for(unsigned int i=0; i<zRealdot.size(); i++)
        foutzdot << zRealdot[i] << endl;

        foutzdot.close();

        cout << "------------------" << endl;
        cout << "The SMC is achieved" << endl;
        // 
        return 0;
        }

        void parse_param(string s){
        string s_backup = s;
        string s_before_space;
        string s_after_space;
        int after_space_ind = 0;
        int i = 0;
        char str[64][64];
        char **param = (char **) malloc(sizeof(char*)*64);
        string s_pre = "";
        while (after_space_ind!=-1) {
            
            after_space_ind = s.find(' ');
        //        cout << "after_space_ind: "<<after_space_ind << endl;
            
            s_before_space = s.substr(0, after_space_ind);
            
            strcpy(str[i], s_before_space.c_str());
        //        cout << str[i] << endl;
            
            param[i] = new char[64];
            strcpy(param[i], str[i]);
            //cout << param[i] << endl;
            
            s = s.substr(after_space_ind+1, s_backup.size());
           cout << "s: "<<s << endl;
            s_pre = s;
        //        cout << "s_pre: "<<s_pre << endl;
            
            i++;
        }

        //    struct svm_param par;
        arc = i;
        arv = param;
        //
        //    printf("____________%d\n",i);
        return;

        }

