clc
clear all
close all

% footx = importdata('footx.txt');
% footxdot = importdata('footxdot.txt');
% 
% footz = importdata('footz.txt');
% footzdot = importdata('footzdot.txt');
% 
% footx = footx + 0.01 * randn(size(footx,1),1);
% footxdot = footxdot + 0.01 * randn(size(footxdot,1),1);
% 
% footz = footz + 0.01 * randn(size(footz,1),1);
% footzdot = footzdot + 0.01 * randn(size(footzdot,1),1);

% save('Randomfootx2.txt','footx','-ASCII');
% save('Randomfootxdot2.txt','footxdot','-ASCII');
%  
% save('Randomfootz2.txt','footz','-ASCII');
% save('Randomfootzdot2.txt','footzdot','-ASCII');

footx = importdata('Randomfootx2.txt');
footxdot = importdata('Randomfootxdot2.txt');
footz = importdata('Randomfootz2.txt');
footzdot = importdata('Randomfootzdot2.txt');

% the surface
T1 = 0.5;
T2 = 0.4;
omega1 = 2 * pi / (2 * T1);
omega2 = 2 * pi / (2 * T2);

% Z Direction
T1 = 0.5;
T2 = 0.4;
omega1 = 2 * pi / (2 * T1);
omega2 = 2 * pi / (2 * T2);

% Z Direction
% Phase 1
ZInitial = 0;
ZMax = 0.4;
ZStairs = 0.2;
 
inc_t = 0.0025;

T1 = 0.5;
T2 = 0.4;
% omega1 = 2 * pi / (2 * T1);

% X Direction
XInitial = 0;
XMax = 0.5;
T = T1 + T2;

NumDataX = T/inc_t;%(ZMax - ZInitial) / inc_z + 1;
omega1 = 3 * pi / (4 * XMax);
omega2 = 2 * pi / (2 * T);

% S2 = (footzdot/(ZMax * XMax * omega1 * omega2 * cos(omega1 * footx) /2))^2 ...
%     + ((footx - XMax/2)/(XMax/2))^2 -1;

S1_1 = (footxdot/(XMax/2 * omega2)).^2 + ((footx - XMax/2)/(XMax/2)).^2 - 1;
% S1_2 = footzdot - ZMax * omega1 * cos(omega1 * footx) .* footxdot;

% s1 = S1_1.^2 + S1_2.^2;
s1 = S1_1;

lambda = 1;
S2 = footz - ZMax * sin(omega1 * footx);
S2dot = footzdot - ZMax * omega1 * cos(omega1 * footx) .* footxdot;

s2 = lambda * S2 + S2dot;%


fileID = fopen('FootSurf1','w');

for i = 1 : size(footx,1)
formatSpec = '%.16f 1:%.16f 2:%.16f\n';
fprintf(fileID, formatSpec, s1(i), footx(i), footxdot(i));
end
fclose(fileID);

fileID = fopen('FootSurf2','w');

for i = 1 : size(footx,1)
formatSpec = '%.16f 1:%.16f 2:%.16f 3:%.16f 4:%.16f\n';
fprintf(fileID, formatSpec, s2(i), footx(i), footxdot(i), footz(i), footzdot(i));
end
fclose(fileID);


% x = 0:0.01:1;
% y = ones(length(x), 1);
% 
% fileID = fopen('DataNewTest','w');
% 
% for i = 1 : size(x,2)
% formatSpec = '%.16f 1: %.16f\n';
% fprintf(fileID, formatSpec, y(i), x(i));
% end
% 
% fclose(fileID);


